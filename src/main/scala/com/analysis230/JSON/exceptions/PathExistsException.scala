package com.analysis230.JSON.exceptions

class PathExistsException(private val msg: String, private val path: String) extends Exception(msg)


object  PathExistsException
{
	def apply(path:String) = new PathExistsException("The path: `%s` already exists in the given object".format(path), path)
}