package com.analysis230.JSON.exceptions

class InvalidValueTypeException(private val msg: String) extends Exception(msg)

object  InvalidValueTypeException
{
	def apply(msg: String) = new InvalidValueTypeException(msg)
}
