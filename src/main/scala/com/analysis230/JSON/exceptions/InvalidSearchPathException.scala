package com.analysis230.JSON.exceptions

class InvalidSearchPathException(private val msg: String, private val path: String) extends Exception(msg)

object  InvalidSearchPathException
{
	def apply(path:String) = new InvalidSearchPathException("The path: `%s` does not exist in the given object".format(path), path)
}

