package com.analysis230.JSON

import com.analysis230.JSON.exceptions.InvalidValueTypeException

object Commons
{
	def getJSONElement(value: Any): JSONelement =
	{
		value match
		{
			case a: String => STRING(a)
			case a: Long => LONG(a)
			case a: Int => LONG(a)
			case a: Double => DOUBLE(a)
			case a: Float => DOUBLE(a)
			case a: Seq[Any] => new ARRAY(a.map(getJSONElement(_)): _*)
			case true => TRUE
			case false => FALSE
			case null => NULL
			case a: JSONelement => a
			case _ => throw InvalidValueTypeException("value type: %s is not supported".format(value.getClass.toString))
		}
	}
}
