package com.analysis230.JSON

private[JSON] object JSONCompiler
{
	def apply(code: String): Either[WorkflowCompilationError, JSONobject] =
	{
		for
			{
			tokens <- WorkflowLexer(code).right
			ast <- WorkflowParser(tokens).right
		} yield ast
	}
}

object StringToJSON
{
	def apply(jsonString: String): OBJECT =
	{
		try
		{
			val A = JSONCompiler(jsonString).right.get
			
			A.asInstanceOf[OBJECT]
			
		}
		catch
		{
			case e: java.util.NoSuchElementException =>
			{
				System.err.println(JSONCompiler(jsonString))
				throw e
			}
		}
	}
}