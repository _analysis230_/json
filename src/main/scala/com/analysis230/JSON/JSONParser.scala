package com.analysis230.JSON

import com.analysis230.JSON.exceptions._
import javax.swing.JPopupMenu.Separator

import scala.collection.{immutable, mutable}
import scala.util.parsing.combinator.Parsers
import scala.util.parsing.input._


sealed trait JSONobject extends java.io.Serializable with JSONelement


private[JSON] class KEYVALUE(private val key: STRING, private val value: JSONelement)
{
	def getKey: String = key.get
	
	def getValue: JSONelement = value
}

private[JSON] class ARRAY(private val values: JSONelement*) extends JSONobject
{
	override def get: List[Any] =
	{
		values.map(_.get).toList
	}
	
	override def toString: String = "[" + this.get.map(_.toString).mkString(",") + "]"
	
	override def getType: String =
	{
		"Array(%s)".format(values.map(_.getType).toSet.mkString(", "))
	}
}


class OBJECT(private val valMap: Map[String, JSONelement]) extends JSONobject
{
	override def get: OBJECT = this
	
	//gets the value by key. e.g. obj.getByKey("key1.key2", ".")
	override def toString =
	{
		"{%s}".format(
			this.valMap.map(tup =>
			{
				"\"%s\":%s".format(tup._1, tup._2.toString)
			}).mkString(",")
		)
	}
	
	private def getByKey(path: Array[String], fullPath: String): Any =
	{
		
		if (path.length == 1)
		{
			try
			{
				this.valMap(path(0)).get
			}
			catch
			{
				case e: java.util.NoSuchElementException => throw InvalidSearchPathException(fullPath)
			}
		}
		
		else
		{
			val obj = this.valMap(path(0)).get
			
			obj match
			{
				case obj: OBJECT => obj.getByKey(path.drop(1), fullPath)
				case _ => throw InvalidSearchPathException(fullPath)
			}
		}
		
		
	}
	
	def getByKey(key: String, separator: String): Any =
	{
		val path = key.split(separator)
		getByKey(path, key)
	}
	
	def getByKey(key: String): Any = getByKey(key, "\\.")
	
	//getByKey but returns your desired data type
	def getByKeyAs[T](key: String): T = getByKeyAs(key, "\\.")
	
	//overloading the above method
	def getByKeyAs[T](key: String, separator: String): T = getByKey(key, separator).asInstanceOf[T]
	
	def getType: String =
	{
		"{%s}".format(
			this.valMap.map(tup =>
			{
				tup._2 match
				{
					case a: OBJECT => "\"%s\":%s".format(tup._1, tup._2.getType)
					case _ => "\"%s\":\"%s\"".format(tup._1, tup._2.getType)
				}
			}).mkString(",")
		)
	}
	
	def getBeautifiedType: String =
	{
		val obj = StringToJSON(this.getType)
		
		obj.getBeautifiedString
	}
	
	//returns a json object with only the selcted fields.  might have problems when you try to select nested fields, please confirm
	def select(fields: String*): OBJECT =
	{
		var valMap = Map[String, JSONelement]()
		
		for (i <- fields.toList)
		{
			val temp = this.valMap.get(i)
			if (temp == None)
			{
				println("Field " + i + " does not exist")
				System.exit(1)
			}
			
			valMap += i -> this.valMap(i)
		}
		
		new OBJECT(valMap)
	}
	
	//overloading the above method
	def select(fields: List[String]): OBJECT = select(fields: _*)
	
	//returns all keys from the current object as a Set[String]
	def getKeys: Set[String] = this.valMap.keySet
	
	//return a HashMap made from the object
	def getMap: Map[String, JSONelement] = this.valMap
	
	//add or replace a value at the given key
	private def addOrReplace(path: Array[String], value: JSONelement, fullPath: String): OBJECT =
	{
		if (path.length == 1)
		{
			new OBJECT(this.valMap + (path(0) -> value))
		}
		else
		{
			val obj = this.valMap(path(0)).get
			
			obj match
			{
				case obj: OBJECT => obj.addOrReplace(path.drop(1), value, fullPath)
				case _ => throw InvalidSearchPathException(fullPath)
			}
			
		}
	}
	
	def addOrReplace(key: String, value: Any, separator: String): OBJECT =
	{
		addOrReplace(key.split(separator), Commons.getJSONElement(value), key)
	}
	
	def addOrReplace(key: String, value: Any): OBJECT = addOrReplace(key, value, "\\.")
	
	def pathExists(key: String, separator: String): Boolean =
	{
		try
		{
			getByKey(key, separator)
			true
		}
		catch
		{
			case e: InvalidSearchPathException => false
		}
	}
	
	def pathExists(key: String): Boolean = pathExists(key, "\\.")
	
	def add(key: String, value: Any, separator: String): OBJECT =
	{
		if (pathExists(key, separator))
			throw PathExistsException(key)
		else
			addOrReplace(key.split(separator), Commons.getJSONElement(value), key)
		
	}
	
	def add(key: String, value: Any): OBJECT = add(key, value, "\\.")
	
	private def remove(path: Array[String], fullPath: String): OBJECT =
	{
		val obj = this.valMap.get(path(0))
		if (path.length == 1)
		{
			if (obj != None)
			{
				new OBJECT(this.valMap - path(0))
			}
			else
			{
				throw InvalidSearchPathException(fullPath)
			}
			
		}
		else
		{
			obj.get match
			{
				case a: OBJECT => this.addOrReplace(path(0), a.remove(path.drop(1), fullPath))
				case _ => throw InvalidSearchPathException(fullPath)
			}
		}
	}
	
	//removes the given key and its value
	def remove(key: String, separator: String): OBJECT =
	{
		val path = key.split(separator)
		remove(path, key)
	}
	
	def remove(key: String): OBJECT = remove(key, "\\.")
	
	
	//renames the given key. Can also restructure e.g. if you give key1.key2 -> key3 it would make a new field key3 and un-nest the object
	def rename(oldKey: String, newKey: String, separator: String): OBJECT =
	{
		val value = this.getByKey(oldKey)
		
		val tempObj = this.remove(oldKey, separator)
		
		tempObj.add(newKey, value, separator)
	}
	
	def rename(oldKey: String, newKey: String): OBJECT = rename(oldKey, newKey, "\\.")
	
	
	//merges the two JSON objects renames any keys that are the same in both the objects.
	def join(B: OBJECT): OBJECT =
	{
		val A = this
		
		val Akeys = A.getKeys
		val Bkeys = B.getKeys
		val sameKeys = Akeys.intersect(Bkeys)
		
		val (leftObj, rightObj) = sameKeys.foldLeft((A, B))((objs, key) =>
		{
			(objs._1.rename(key, "_left_" + key), objs._2.rename(key, "_right_" + key))
		})
		
		
		new OBJECT(leftObj.getMap ++ rightObj.getMap)
	}
	
	//returns a beautified version of the JSON object
	private def getBeautifiedString(prepend: String): String =
	{
		"{\n%s\n%s}".format(
			this.valMap.map(tup =>
			{
				tup._2 match
				{
					case a: OBJECT => "%s\t\"%s\":%s".format(prepend, tup._1, a.getBeautifiedString(prepend + "\t" + " " * (tup._1.length + 3)))
					case _ => "%s\t\"%s\":%s".format(prepend, tup._1, tup._2.toString)
				}
			}).mkString(",\n"), prepend
		)
	}
	
	def getBeautifiedString: String = getBeautifiedString("")
}

private[JSON] object OBJECT
{
	def apply(keyvalues: KEYVALUE*) =
	{
		
		new OBJECT(keyvalues.map(i => (i.getKey, i.getValue)).toMap)
	}
}

private[JSON] object WorkflowParser extends Parsers
{
	override type Elem = JSONToken
	
	class WorkflowTokenReader(tokens: Seq[JSONToken]) extends Reader[JSONToken]
	{
		override def first: JSONToken = tokens.head
		
		override def atEnd: Boolean = tokens.isEmpty
		
		override def pos: Position = NoPosition
		
		override def rest: Reader[JSONToken] = new WorkflowTokenReader(tokens.tail)
	}
	
	
	private def string: Parser[STRING] =
	{
		accept("string", {
			case str: STRING => str
		})
	}
	
	
	private def long: Parser[LONG] =
	{
		accept("long", { case nmbr: LONG => nmbr })
	}
	
	private def double: Parser[DOUBLE] =
	{
		accept("double", { case nmbr: DOUBLE => nmbr })
	}
	
	def program: Parser[JSONobject] =
	{
		phrase(obj | emptyobj)
	}
	
	def obj: Parser[JSONobject] =
	{
		(CURLYBRACSTART ~ keyvalue ~ (COMMA ~ keyvalue).* ~ CURLYBRACSTOP) ^^
		  {
			  //case a => System.err.println(a); EMPTYARRAY
			  case (((CURLYBRACSTART ~ a) ~ b) ~ CURLYBRACSTOP) =>
			  {
				  OBJECT(a :: b.flatMap
				  { case (x ~ y) => List(y) }: _*)
			  }
		  }
	}
	
	def emptyobj: Parser[JSONobject] =
	{
		(CURLYBRACSTART ~ CURLYBRACSTOP) ^^ (_ => OBJECT(List().asInstanceOf[List[KEYVALUE]]: _*))
	}
	
	def emptyarray: Parser[JSONobject] =
	{
		
		(SQRBRACSTART ~ SQRBRACSTOP) ^^ (_ => new ARRAY(List().asInstanceOf[List[JSONelement]]: _*))
	}
	
	def array: Parser[JSONobject] =
	{
		(SQRBRACSTART ~ (string | long | double | NULL | FALSE | TRUE | array | obj | emptyarray | emptyobj) ~ (COMMA ~ (string | long | double | NULL | FALSE | TRUE | array | obj | emptyarray | emptyobj)).* ~ SQRBRACSTOP) ^^
		  {
			  case (((SQRBRACSTART ~ a) ~ b) ~ SQRBRACSTOP) =>
			  {
				  new ARRAY(a :: b.flatMap
				  { case (x ~ y) => List(y) }: _*)
			  }
		  }
	}
	
	
	def keyvalue: Parser[KEYVALUE] =
	{
		(string ~ COLON ~ (string | long | double | FALSE | NULL | TRUE | array | obj | emptyarray | emptyobj)) ^^
		  { case k1 ~ COLON ~ v1 => new KEYVALUE(k1, v1) }
	}
	
	case class WorkflowParserError(msg: String) extends WorkflowCompilationError
	
	def apply(tokens: Seq[JSONToken]): Either[WorkflowParserError, JSONobject] =
	{
		val reader = new WorkflowTokenReader(tokens)
		program(reader) match
		{
			case NoSuccess(msg, next) => Left(WorkflowParserError(msg))
			case Success(result, next) => Right(result)
		}
	}
}
