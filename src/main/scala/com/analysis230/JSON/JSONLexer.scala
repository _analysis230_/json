package com.analysis230.JSON

import scala.util.parsing.combinator.RegexParsers

trait JSONelement
{
	def get: Any
	def getType: String
}
sealed trait JSONToken extends java.io.Serializable with JSONelement
{
	def getString: String
}

private[JSON] class STRING(private val str: String) extends JSONToken
{
	override def get: String = str
	
	override def getString: String = str
	
	override def toString: String = "\"" + str + "\""
	
	override def getType: String = "string"
}

object STRING
{
	def apply(value: String) =
	{
		new STRING(value)
	}
}

private[JSON] class LONG(private val nmbr: String) extends JSONToken
{
	override def get: Long = nmbr.toLong
	
	override def getString: String = nmbr
	
	private[JSON] def getLong: Long = nmbr.toLong
	
	def getInt: Int = nmbr.toInt
	
	override def getType: String = "long"
	
	override def toString: String = nmbr.toLong.toString
}

object LONG
{
	def apply(value: Long) =
	{
		new LONG(value.toString)
	}
	
}

private[JSON] class DOUBLE(private val nmbr: String) extends JSONToken
{
	override def get: Double = nmbr.toDouble
	
	override def getString: String = nmbr
	
	def getFloat: Float = nmbr.toFloat
	
	def getDouble: Double = nmbr.toDouble
	
	def getType: String = "double"
	
	override def toString: String = nmbr.toDouble.toString
}

object DOUBLE
{
	def apply(value: Double) =
	{
		new DOUBLE(value.toString)
	}
}

private[JSON] object SQRBRACSTART extends JSONToken
{
	override def get: String = "["
	
	override def getString: String = this.get
	
	override def toString: String = "["
	
	override def getType: String = "string"
	
}

private[JSON] object SQRBRACSTOP extends JSONToken
{
	override def get: String = "]"
	
	override def getString: String = this.get
	
	override def toString: String = "]"
	
	override def getType: String = "string"
	
}

private[JSON] object CURLYBRACSTART extends JSONToken
{
	override def get: String = "{"
	
	override def getString: String = this.get
	
	override def toString: String = "{"
	
	override def getType: String = "string"
	
}

private[JSON] object CURLYBRACSTOP extends JSONToken
{
	override def get: String = "}"
	
	override def getString: String = this.get
	
	override def toString: String = "}"
	
	override def getType: String = "string"
	
}

private[JSON] object COLON extends JSONToken
{
	override def get: String = ":"
	
	override def getString: String = this.get
	
	override def toString: String = ":"
	
	override def getType: String = "string"
	
}

private[JSON] object COMMA extends JSONToken
{
	override def get: String = ","
	
	override def getString: String = this.get
	
	override def toString: String = ","
	
	override def getType: String = "string"
	
}

private[JSON] object TRUE extends JSONToken
{
	override def get: String = "true"
	
	override def getString: String = this.get
	
	override def toString: String = "true"
	
	override def getType: String = "string"
	
}

private[JSON] object FALSE extends JSONToken
{
	override def get: String = "false"
	
	override def getString: String = this.get
	
	override def toString: String = "false"
	
	override def getType: String = "string"
	
}

private[JSON] object NULL extends JSONToken
{
	override def get: String = "null"
	
	override def getString: String = this.get
	
	override def toString: String = "null"
	
	override def getType: String = "string"
	
}


private[JSON] trait WorkflowCompilationError

private[JSON] case class WorkflowLexerError(msg: String) extends WorkflowCompilationError

private[JSON] object WorkflowLexer extends RegexParsers
{
	override def skipWhitespace = true
	
	override val whiteSpace = "[ \n\t\r\f]+".r
	
	def string: Parser[STRING] =
	{
		"\"([^\\\\\"]|(\\\\([\"\\\\/bfnrt]|(u[0-9A-F]{4}))))*\"".r ^^
		  { str => STRING(str.drop(1).dropRight(1)) }
	}
	
	
	def long: Parser[LONG] =
	{
		"-?[0-9]+".r ^^
		  {
			  str => new LONG(str)
		  }
	}
	
	def double: Parser[DOUBLE] =
	{
		"-?[0-9]+((\\.[0-9]+)((e|E)(\\+|-)?[0-9]+)|((e|E)(\\+|-)?[0-9]+)|(\\.[0-9]+))".r ^^
		  {
			  
			  str => new DOUBLE(str)
		  }
	}
	
	def sqrbracstart = "[" ^^ (_ => SQRBRACSTART)
	
	def sqrbracstop = "]" ^^ (_ => SQRBRACSTOP)
	
	def curlybracstart = "{" ^^ (_ => CURLYBRACSTART)
	
	def curlybracstop = "}" ^^ (_ => CURLYBRACSTOP)
	
	def colon = ":" ^^ (_ => COLON)
	
	def comma = "," ^^ (_ => COMMA)
	
	def tru = "true" ^^ (_ => TRUE)
	
	def fals = "false" ^^ (_ => FALSE)
	
	def nul = "null" ^^ (_ => NULL)
	
	
	def tokens: Parser[List[JSONToken]] =
	{
		phrase(rep1(string | double | long | sqrbracstart | sqrbracstop | curlybracstart | curlybracstop | colon
		  | comma | tru | fals | nul))
	}
	
	def apply(code: String): Either[WorkflowLexerError, List[JSONToken]] =
	{
		parse(tokens, code) match
		{
			case NoSuccess(msg, next) => Left(WorkflowLexerError(msg))
			case Success(result, next) => Right(result)
		}
	}
}
