<h1>analysis230 JSON Library for Scala</h1>


*Scala Version: 2.11.8*

<h3>How to Build</h3>
You can use either *SBT* or *maven* to build. *SBT* is recommended

for sbt:
 `sbt clean package`
 
 for maven:
 `mvn clean package`
 
 This library was built as a string processing engine to be able to handle JSON string and do various operations on the same 
 
 <h3>How to Create JSON Objects</h3>
 
 It is fairly simple to create a JSON object from a JSON string in this library. The ability to create JSON objects from scala.collection.immutable.Map would soon be added
 ```Scala
 import com.analysis230.JSON.StringToJSON
 
 val a = StringToJSON("{\"key\":123, \"key1\":\"Hello\",\"key2\":567, \"key3\":{\"key2\":\"567\", \"key5\":{\"key2\":\"567\"}}, \"key4\":[1,2,3], \"key5\":[]}")
        
 println(a)
 println("\n")
 println(a.getBeautifiedString)
```
The output of the above code should look like this:

    {"key4":[1,2,3],"key5":[],"key":123,"key1":"Hello","key2":567,"key3":{"key2":"567","key5":{"key2":"567"}}}
    

    {
        "key4":[1,2,3],
        "key5":[],
        "key":123,
        "key1":"Hello",
        "key2":567,
        "key3":{
                "key2":"567",
                "key5":{
                        "key2":"567"
                       }
               }
    }
To get any element you can simply use the *getByKey* method:
```Scala
import com.analysis230.JSON.StringToJSON

val a = StringToJSON("{\"key\":123, \"key1\":\"Hello\",\"key2\":567, \"key3\":{\"key2\":\"567\", \"key5\":{\"key2\":\"567\"}}, \"key4\":[1,2,3], \"key5\":[]}")
		
println(a.getByKey("key"))
println(a.getByKey("key3>key2", ">"))  //You can also fetch elements that are nested by providing the keys and a separator
println(a.getByKey("key3.key2"))  //The default separator is "."
```

```
123
567
567
```

This function returns objects with type *Any*, if you require the object to be of a particular type you can use the *getByKeyAs[T]* method which does the same thing but returns the element as an object of whatever type you want.

```Scala
import com.analysis230.JSON.StringToJSON
val a = StringToJSON("{\"key\":123, \"key1\":\"Hello\",\"key2\":567, \"key3\":{\"key2\":\"567\", \"key5\":{\"key2\":\"567\"}}, \"key4\":[1,2,3], \"key5\":[]}")
		
println(a.getByKeyAs[Long]("key"))
println(a.getByKeyAs[Long]("key3>key2", ">"))  //You can also fetch elements that are nested by providing the keys and a separator
println(a.getByKeyAs[String]("key1"))  //The default separator is "."
```

<h3>Key Features</h3>
* Supports dot notation
* Quite type agnostic
* Adding new elements or Replacing old elements
   ```
   a.add("key8", "SomeNewValue")   //throws error if the key actually exists
   a.addOrReplace("key8", "SomeNewValue")
  ```
* Renaming/Restructuring the keys
    ```
    a.rename("key3.key2", "key9")
    ```
* Removing keys
* Merging two JSON objects into one

<h3>Next steps</h3>
* adding a test suit
* functions to add:
    * map
    * mapKeys
    * mapValues
    * reduce
    * reduceKeys
    * reduceValues
    * flatMap
* Stronger list processing
* ability to create objects from *scala.collection.immutable.Map*