
ThisBuild / organization := "com.analysis230"

ThisBuild / version := "4.0.0-Beta"

ThisBuild / scalaVersion := "2.11.8"


//properties ......
val scala_tools_version = "2.11"
val spark_version = "2.4.0"
val scala_version = "2.11.8"


val `spark-core` = "org.apache.spark" % ("spark-core_" + scala_tools_version) % spark_version
val `spark-sql` = "org.apache.spark" % ("spark-sql_" + scala_tools_version) % spark_version
val `scala-parser-combinators` = "org.scala-lang" % "scala-parser-combinators" % "2.11.0-M4"
val `scala-library` = "org.scala-lang" % "scala-library" % scala_version


lazy val root = (project in file("."))
  .settings(
	  name := "JSON",
	  libraryDependencies ++= Seq(`spark-core`,`spark-sql`,`scala-parser-combinators`,`scala-library`)
  )